using UnityEngine;

namespace HoodedCrow.Core
{
    public class Singleton<T> : CachedMonoBehaviour where T : CachedMonoBehaviour
    {
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (T) FindObjectOfType<T>();
                    if (_instance == null)
                    {
                        GameObject go = new GameObject();
                        go.name = typeof(T).ToString();
                        _instance = go.AddComponent<T>();
                        
                        DontDestroyOnLoad(go);
                    }
                }

                return _instance;
            }
        }
        
        private static T _instance = null;
    }
}