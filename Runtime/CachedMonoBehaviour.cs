using System;
using UnityEditor.PackageManager;
using UnityEngine;

namespace HoodedCrow.Core
{
    public class CachedMonoBehaviour :MonoBehaviour
    {
        public new Transform transform { get; private set; }
        protected virtual void Awake()
        {
            transform = this.GetComponent<Transform>();
        }
    }
}