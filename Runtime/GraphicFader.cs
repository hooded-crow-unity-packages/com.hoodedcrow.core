using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace HoodedCrow.Core
{
    public static class GraphicFader
    {
        public static IEnumerator FadeOut(Graphic graphic, float time)
        {
            float timeLeft = time;
            Color color = graphic.color;

            while (timeLeft > 0)
            {
                timeLeft -= Time.deltaTime;
                color.a -= Time.deltaTime/time;
                graphic.color = color;
                yield return 0;
            }

            color.a = 0;
            graphic.color = color;
        }
        
        public static IEnumerator FadeIn(Graphic graphic, float time)
        {
            float timeLeft = time;
            Color color = graphic.color;

            while (timeLeft > 0)
            {
                timeLeft -= Time.deltaTime;
                color.a += Time.deltaTime/time;
                graphic.color = color;
                yield return 0;
            }

            color.a = 1;
            graphic.color = color;
        }
    }
}