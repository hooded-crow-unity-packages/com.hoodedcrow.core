using UnityEngine;

namespace HoodedCrow.Core
{
    public class AppManager : Singleton<AppManager>
    {
        public static bool IsDebugBuild => Application.isEditor || Debug.isDebugBuild;
    
        public T GetGameManager<T>() where T:AGameManager => (T)_gameManager;
        private AGameManager _gameManager;

        public void RegisterGameManager(AGameManager gameManager)
        {
            if (_gameManager != null)
            {
                _gameManager.Uninitialize();
            }

            _gameManager = gameManager;
            _gameManager.Initialize();
        }
    }
}