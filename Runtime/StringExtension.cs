namespace HoodedCrow.Core
{
    public static class StringExtension
    {
        private const string BOLD_FORMAT = "<b>{0}</b>";
        private const string ITALIC_FORMAT = "<i><{0}/i>";
        private const string SIZE_FORMAT = "<size={1}>{0}</size>";
        private const string COLOR_FORMAT = "<color={1}>{0}</color>";
        
        public static string Bold(this string str)
        {
            return string.Format(BOLD_FORMAT, str);
        }
        public static string Italic(this string str)
        {
            return string.Format(ITALIC_FORMAT, str);
        }

        public static string Size(this string str, int size)
        {
            return string.Format(SIZE_FORMAT, str, size);
        }

        public static string Color(this string str, string color)
        {
            return string.Format(COLOR_FORMAT, str, color);
        }
    }
}