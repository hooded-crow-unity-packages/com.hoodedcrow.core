using System.Collections.Generic;
using UnityEngine;

namespace HoodedCrow.Core.Pool
{
    public class Pool<T> : CachedMonoBehaviour where T : PoolElement
    {
        private int _nextID = 0;

        [SerializeField]
        private T _elementPrefab = null;

        [Space(10)]
        [SerializeField]
        private bool _canExpand = false;

        [SerializeField]
        private int _expandAmount = 4;

        private List<T> _freeElements = new List<T>();
        private List<T> _usedElements = new List<T>();

        public void Init(int initialAmount = 8)
        {
            SpawnNewObjects(initialAmount);
        }

        public T GetObjectFromPool()
        {
            if (_freeElements.Count == 0)
            {
                HandleEmptyPool();
            }

            T element = _freeElements[0];
            _freeElements.RemoveAt(0);
            _usedElements.Add(element);

            return element;
        }

        public void ReturnObjectToPool(T element)
        {
            T go = element;

            _usedElements.Remove(element);
            _freeElements.Add(element);
            go.transform.SetParent(this.transform);
            go.transform.position = new Vector3();
        }

        
        
        private void HandleEmptyPool()
        {
            if (_canExpand)
            {
                SpawnNewObjects(_expandAmount);
            }

            T element = _usedElements[0];
            _usedElements.RemoveAt(0);
            _freeElements.Add(element);
        }

        private void SpawnNewObjects(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                T element = SpawnNewObject();
                _freeElements.Add(element);
            }
        }

        private T SpawnNewObject()
        {
            T element = Instantiate<T>(_elementPrefab, this.transform, true);

            element.gameObject.SetActive(false);
            element.transform.position = new Vector3();
            element.name = element.name + " " + _nextID.ToString();
            _nextID++;

            return element;
        }
    }
}