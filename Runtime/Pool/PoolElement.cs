namespace HoodedCrow.Core.Pool
{
    public class PoolElement : CachedMonoBehaviour
    {
        private Pool<PoolElement> _pool = null;

        public void SetPool(Pool<PoolElement> pool)
        {
            _pool = pool;
        }

        public void ReturnToPool()
        {
            _pool.ReturnObjectToPool(this);
        }
    }
}