namespace HoodedCrow.Core
{
    public interface IModuleManager
    {
        void Initialize(AGameManager gameManager);
        void Uninitialize(AGameManager gameManager);
    }
}