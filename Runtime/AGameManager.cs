using System;
using System.Collections.Generic;
using UnityEngine;

namespace HoodedCrow.Core
{
    public abstract class AGameManager : MonoBehaviour
    {
        protected readonly Dictionary<Type, IModuleManager> _modules = new Dictionary<Type, IModuleManager>();

        private void Awake()
        {
            AppManager.Instance.RegisterGameManager(this);
        }

        public abstract void Initialize();

        public abstract void Uninitialize();

        public void RegisterModule(IModuleManager module)
        {
            if (_modules.ContainsKey(module.GetType()))
            {
                if (AppManager.IsDebugBuild)
                {
                    Debug.LogWarning($"#Game Manager# Module {module.GetType()} already registered");
                }

                return;
            }

            _modules[module.GetType()] = module;
            if (AppManager.IsDebugBuild)
            {
                Debug.Log($"#Game Manager#Registered module {module.GetType()}");
            }

            module.Initialize(this);
        }

        public T GetModule<T>() where T : IModuleManager
        {
            return (T) _modules[typeof(T)];
        }

        public void UnregisterModule(IModuleManager moduleManager)
        {
            if (_modules.ContainsKey(moduleManager.GetType()))
            {
                if (AppManager.IsDebugBuild)
                {
                    Debug.LogWarning($"#Game Manager#Modules {moduleManager.GetType()} isn't registered");
                }

                return;
            }

            IModuleManager module = _modules[moduleManager.GetType()];
            module.Uninitialize(this);
            _modules.Remove(moduleManager.GetType());
            if (AppManager.IsDebugBuild)
            {
                Debug.Log($"#Game Manager#Unregistered module {module.GetType()}");
            }
        }
    }
}